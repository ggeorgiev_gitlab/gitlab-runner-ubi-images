#!/bin/bash

tag() {
    # shellcheck source=scripts/_tags.sh
    source "${SCRIPTS_DIR}/_tags.sh"

    printMessage 1 "Tagging image '$RUNNER_EXISTING_TAG' as '$RUNNER_TAG'"
    docker tag "$RUNNER_EXISTING_TAG" "$RUNNER_TAG"

    printMessage 1 "Tagging image '$EXISTING_HELPER_TAG' as '$HELPER_TAG'"
    docker tag "$EXISTING_HELPER_TAG" "$HELPER_TAG"
}