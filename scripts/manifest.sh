#!/bin/bash

PLATFORMS=${PLATFORMS:-linux/amd64}

manifest() {
    RUNNER_MANIFEST="$BUILD_REPOSITORY/gitlab-runner-ocp:$RELEASE"
    HELPER_MANIFEST="$BUILD_REPOSITORY/gitlab-runner-helper-ocp:$RELEASE"

    echo "$CI_REGISTRY_PASSWORD" | docker login --username "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"

    echo $(./scripts/get_manifest.py "$PLATFORMS" "$RUNNER_MANIFEST") | xargs docker manifest create
    docker manifest push "$RUNNER_MANIFEST"

    echo $(./scripts/get_manifest.py "$PLATFORMS" "$HELPER_MANIFEST" 1) | xargs docker manifest create
    docker manifest push "$HELPER_MANIFEST"
}