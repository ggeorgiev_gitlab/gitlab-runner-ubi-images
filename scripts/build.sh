RUNNER_REVISION="${RUNNER_REVISION:-}"
GIT_LFS_VERSION=2.13.3-2.el8

_imageName() {
  local IMAGE_NAME="${1}"
  local TAG="${2}"

  printf "%s/%s:%s" "${BUILD_REPOSITORY}" "${IMAGE_NAME}" "${TAG}"
}

_buildImage() {
  local IMAGE_NAME="${1}"
  local TARGET_IMAGE="${2}"
  local CONTEXT="${3}"
  local VERSION="${4}"
  local DOCKERFILE="Dockerfile"

  if [[ -n $DOCKERFILE_PREPEND ]]; then
    local DOCKERFILE="${DOCKERFILE_PREPEND}.Dockerfile"
  fi

  printMessage 0 "Building ${TARGET_IMAGE} image for ${OSARCH}"

  {
    docker buildx build \
           --platform "${OSARCH}" \
           -t "${TARGET_IMAGE}" \
           --output type=image \
           --build-arg "BASE_IMAGE=${BASE_IMAGE}" \
           --build-arg "VERSION=${VERSION}" \
           --build-arg "GIT_LFS_VERSION=${GIT_LFS_VERSION}" \
           "${DOCKER_OPTS[@]}" \
           -f "${CONTEXT}/${DOCKERFILE}" \
           "${CONTEXT}" |& tee "${LOGS_DIR}/${IMAGE_NAME}.log"
  } || {
    echo "${IMAGE_NAME}" >> "${LOGS_DIR}/failed.log"
  }
}

_generateValuesYAML() {
  local MAIN_IMAGE="${1}"
  local HELPER_IMAGE="${2}"

  printMessage 0 "Generating ${VALUES_YAML_FILE} file"

  local USER_ID
  USER_ID="$(docker run --rm --entrypoint sh "${MAIN_IMAGE}" -c "id -u gitlab-runner")"

  local GROUP_ID
  GROUP_ID="$(docker run --rm --entrypoint sh "${MAIN_IMAGE}" -c "id -g nobody")"

  cat > "${VALUES_YAML_FILE}" <<EOF
securityContext:
  runAsUser: ${USER_ID}
  fsGroup: ${GROUP_ID}
image: ${MAIN_IMAGE}
runners:
  helpers:
    image: ${HELPER_IMAGE}

EOF
}

_testImages() {
  local MAIN_IMAGE="${1}"
  local HELPER_IMAGE="${2}"

  docker run --rm "${MAIN_IMAGE}" --version
  docker run --rm --entrypoint /bin/sh "${MAIN_IMAGE}" -c "cat /etc/os-release"
  docker run --rm "${HELPER_IMAGE}" gitlab-runner-helper --version
  docker run --rm "${HELPER_IMAGE}" cat /etc/os-release
}

buildImages() {
  if [[ ! -d "${WORKSPACE}" ]]; then
    echo "Workspace is not initialized. Call 'prepare.sh' first!"
    exit 1
  fi

  # Enter the workspace. From now on everything will be executed
  # in this directory
  cd "${WORKSPACE}" || return 1

  if [[ -z "${RUNNER_REVISION}" && "${ARCH}" == "amd64" ]]; then
    RUNNER_REVISION=$("${RUNNER_BINARY}" --version | grep "Git revision" | sed -r "s/Git revision: //")
  elif [[ -z "${RUNNER_REVISION}" && "${ARCH}" == "ppc64le" ]]; then
    RUNNER_REVISION=$(qemu-ppc64le-static "${RUNNER_BINARY}" --version | grep "Git revision" | sed -r "s/Git revision: //")
  fi

  DOCKER_OPTS=()
  if [ -z "$UBI_VERSION" ]; then
    # When building locally by default use :latest, we can still specify UBI_VERSION manually
    BASE_IMAGE="${UBI_IMAGE}"
  else
    BASE_IMAGE=$(printf "%s:%s" "${UBI_IMAGE}" "${UBI_VERSION}")
  fi
  
  MAIN_IMAGE="$(_imageName gitlab-runner "${ARCH}-${RELEASE}")"
  HELPER_IMAGE="$(_imageName gitlab-runner-helper "${HELPER_ARCH}-${RUNNER_REVISION}")"

  _buildImage gitlab-runner "${MAIN_IMAGE}" "${RUNNER_CONTEXT}" "${RELEASE}" "${OSARCH}"
  _buildImage gitlab-runner-helper "${HELPER_IMAGE}" "${HELPER_CONTEXT}" "${RELEASE}" "${OSARCH}"
  _testImages "${MAIN_IMAGE}" "${HELPER_IMAGE}"

  # For OpenShift we don't need to generate values.yaml. The security context
  # is managed by the OpenShift admin and the Runner Operator uses the project-level
  # security context
  if [[ -z $DOCKERFILE_PREPEND ]]; then
    _generateValuesYAML "${MAIN_IMAGE}" "${HELPER_IMAGE}"
  fi
}
