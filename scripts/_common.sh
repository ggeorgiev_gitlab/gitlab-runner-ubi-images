OSARCH=${OSARCHS:-linux/amd64}
ARCH=$(echo "$OSARCH" | cut -f2 -d/)
HELPER_ARCH=$(if [ "${ARCH}" == "amd64" ]; then echo "x86_64"; else echo "${ARCH}"; fi)

RELEASE=${2:-main}
BUILD_REPOSITORY="${3:-gitlab}"
DOCKERFILE_PREPEND="${4:-""}"

RUNNER_BUCKET_URL="https://gitlab-runner-downloads.s3.amazonaws.com"
RUNNER_URL="${RUNNER_BUCKET_URL}/${RELEASE}"

RELEASE_BASE_URL="${RUNNER_BUCKET_URL}/ubi-images"
RELEASE_URL="${RELEASE_BASE_URL}/${RELEASE}"

GPG_KEY_URL="${RUNNER_BUCKET_URL}/gitlab-runner-key.asc"
GPG_KEY_ID="0x30183AC2C4E23A409EFBE7059CE45ABC880721D4"

WORKSPACE="${BASE_DIR}/build"
CACHE_DIR="/tmp/gitlab-runner-ubi"
LOGS_DIR="${WORKSPACE}/logs"
LICENSES_DIR="${WORKSPACE}/licenses"

RUNNER_BINARY="${WORKSPACE}/gitlab-runner"
HELPER_BINARY="${WORKSPACE}/gitlab-runner-helper"
TINI_BINARY="${WORKSPACE}/tini"

RUNNER_CONTEXT="${WORKSPACE}/runner"
HELPER_CONTEXT="${WORKSPACE}/helper"

CHECKSUM_FILE="${WORKSPACE}/release.sha256"
CHECKSUM_SIGNATURE_FILE="${WORKSPACE}/release.sha256.asc"
VALUES_YAML_FILE="${WORKSPACE}/ubi-values.yaml"

UBI_IMAGE=redhat/ubi8

printMessage() {
  local INDENT="${1}"
  shift 1
  local MSG="${*}"

  local PREFIX="\xe2\x86\x92"
  if [[ ${INDENT} -gt 0 ]]; then
    PREFIX="\xe2\x86\xb3"
    for i in $(seq 1 "${INDENT}"); do
      PREFIX=" ${PREFIX}"
    done
  fi

  echo -e "\033[32;1m${PREFIX} ${MSG}\033[0;0m"
}
