#!/bin/bash

push() {
    # shellcheck source=scripts/_tags.sh
    source "${SCRIPTS_DIR}/_tags.sh"

    echo "$CI_REGISTRY_PASSWORD" | docker login --username "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"

    printMessage 1 "Pushing image '$RUNNER_TAG'"
    docker push "$RUNNER_TAG"

    printMessage 1 "Pushing image '$HELPER_TAG'"
    docker push "$HELPER_TAG"
}