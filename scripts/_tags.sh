#!/bin/bash

export PLATFORM_LOWERCASE
PLATFORM_LOWERCASE=$(echo "$DOCKERFILE_PREPEND" | tr "[:upper:]" "[:lower:]")

export RUNNER_EXISTING_TAG="$BUILD_REPOSITORY/gitlab-runner:$ARCH-$RELEASE"
export RUNNER_TAG="$BUILD_REPOSITORY/gitlab-runner-$PLATFORM_LOWERCASE:$ARCH-$RELEASE"

export EXISTING_HELPER_TAG
EXISTING_HELPER_TAG="$BUILD_REPOSITORY/gitlab-runner-helper:$(docker images --format "{{.Tag}}" "$BUILD_REPOSITORY/gitlab-runner-helper" | head -n 1)"
export HELPER_TAG="$BUILD_REPOSITORY/gitlab-runner-helper-$PLATFORM_LOWERCASE:$HELPER_ARCH-$RELEASE"