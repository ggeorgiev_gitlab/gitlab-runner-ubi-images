#!/bin/bash

S3_BUCKET=${S3_BUCKET:-}
S3_PATH=${S3_PATH:-}
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:-}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:-}

_generateReleaseIndexFile() {
  printMessage 1 "Generating release index file"

  cd build/index || return 1
  release-index-gen -working-directory dist/ \
                    -project-version "${RELEASE}" \
                    -project-git-ref "${CI_COMMIT_REF_NAME:-HEAD}" \
                    -project-git-revision "${CI_COMMIT_SHA:-HEAD}" \
                    -project-name "GitLab Runner - UBI images dependencies" \
                    -project-repo-url "https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-ubi-images" \
                    -gpg-key-env GPG_KEY \
                    -gpg-password-env GPG_PASSPHRASE

  # Integrity selfcheck
  cd dist || return 1
  sha256sum --check --strict release.sha256
  cd .. || return 1
  find dist/
}

_uploadReleaseToS3() {
  if [[ -z "${S3_BUCKET}" ]]; then
    echo "Missing S3_BUCKET variable; skipping upload step"
    return 1
  fi

  if [[ -z "${S3_PATH}" ]]; then
    echo "Missing S3_PATH variable; skipping upload step"
    return 1
  fi

  if [[ -z "${AWS_ACCESS_KEY_ID}" ]]; then
    echo "Missing AWS_ACCESS_KEY_ID variable; skipping upload step"
    return 1
  fi

  if [[ -z "${AWS_SECRET_ACCESS_KEY}" ]]; then
    echo "Missing AWS_SECRET_ACCESS_KEY variable; skipping upload step"
    return 1
  fi

  local S3_URL="s3://${S3_BUCKET}/${S3_PATH}/${RELEASE}"

  printMessage 1 "Syncing the release with S3 bucket ${S3_BUCKET}/${S3_PATH}"

  aws s3 sync dist "${S3_URL}" --acl public-read

  printMessage 1 "Check files at ${RELEASE_BASE_URL}/${RELEASE}/index.html"
}

uploadRelease() {
    _generateReleaseIndexFile
    _uploadReleaseToS3
}