cleanupImageBuild() {
  printMessage 0 "Cleaning up workspace and cache directories"

  rm -rf "${WORKSPACE}"
  rm -rf "${CACHE_DIR}"
}
