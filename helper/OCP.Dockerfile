ARG BASE_IMAGE

FROM ${BASE_IMAGE}

ARG VERSION
LABEL name="GitLab Runner Helper" \
   vendor="GitLab" \
   maintainer="support@gitlab.com" \
   version="${VERSION}" \
   release="${VERSION}" \
   summary="GitLab Runner used to conduct CI/CD jobs on Kubernetes. The helper contains a subset of specialized commands used when running jobs." \
   description="Contains a gitlab-runner-helper binary which is a special compilation of GitLab Runner binary, that contains only a subset of available commands, as well as Git, Git LFS, SSL certificates store. This image will run alongside each build container, in the same pod."

ENV HOME /home/gitlab-runner

# Install security updates that might not be included in the base image.
# Without the latest security updates the image can't pass certification.
RUN dnf --disableplugin=subscription-manager -y update-minimal \
    --security --sec-severity=Important --sec-severity=Critical

ARG GIT_LFS_VERSION
RUN dnf --disableplugin=subscription-manager install -yb --nodocs \
    git \
    hostname \
    perl \
    git-lfs-${GIT_LFS_VERSION}

COPY ./gitlab-runner-build.sh /usr/local/bin/gitlab-runner-build
COPY ./gitlab-runner-helper /usr/bin/gitlab-runner-helper

RUN chmod +x /usr/local/bin/gitlab-runner-build && \
    chmod +x /usr/bin/gitlab-runner-helper

COPY ./licenses /licenses

RUN mkdir -p $HOME && \
    chgrp -R 0 $HOME && \
    chmod -R g=u $HOME

# https://docs.openshift.com/container-platform/4.6/openshift_images/create-images.html#support-arbitrary-user-ids
RUN mkdir -p /builds /cache && \
    chgrp -R 0 /builds && \
    chmod -R g=u /builds && \
    chgrp -R 0 /cache && \
    chmod -R g=u /cache

USER 1001

CMD ["sh"]
